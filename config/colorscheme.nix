{lib, ...}:{
  # TODO: Make colorscheme configurable
  colorscheme = lib.mkOverride 500 "everforest";

  colorschemes = {
    kanagawa.enable = true;
    nord.enable = true;
    tokyonight = {
      enable = true;
      settings = {
        style = "storm";
        light_style = "day";
        transparent = false;
      };
    };
    everforest = {
      enable = true;
      settings = {
        enable_italic = 1;
      };
    };
    catppuccin = {
      enable = true;

      settings = {
        flavour = "mocha";

        # Needed to keep terminal transparency, if any
        transparent_background = false;

        styles = {
          keywords = [ "italic" ];
          conditionals = [ "italic" ];
          functions = [ "italic" ];
          loops = [ "italic" ];
        };
      };
    };
  };
}
