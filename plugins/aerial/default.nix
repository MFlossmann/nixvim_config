{ lib, pkgs, ...}:
{
  extra = {
    packages = [
      (import ./package.nix { inherit lib pkgs; })
    ];

    config = /*lua*/ ''
      require("aerial").setup({
        backends = { "lsp", "treesitter", "markdown", "man" },
        layout = { min_width = 28, placement = "window", default_direction = "prefer_left" },
        show_guides = true,
        filter_kind = false,

        guides = {
          mid_item = "├ ",
          last_item = "└ ",
          nested_top = "│ ",
          whitespace = "  ",
        },
        autojump = true,
        keymaps = {
          ["[y"] = "actions.prev",
          ["]y"] = "actions.next",
          ["[Y"] = "actions.prev_up",
          ["]Y"] = "actions.next_up",
          ["|"] = "actions.jump_vsplit",
          ["\\"] = "actions.jump_split",
          ["{"] = false,
          ["}"] = false,
          ["[["] = false,
          ["]]"] = false,
          ["<C-v>"] = false,
          ["<C-s>"] = false,
        },
        attach_mode = "global",
      })
    '';
  };

  rootOpts.keymaps = [
    {
      mode = "n";
      key = "<leader>lO";
      action.__raw = ''function() require("aerial").open() end'';
      options.desc = "Symbols outline (left)";
    }
    {
      mode = "n";
      key = "<leader>lo";
      action.__raw = ''function() require("aerial").open({direction = "float"}) end'';
      options.desc = "Symbols outline (floating)";
    }
    {
      mode = "n";
      key = "]S";
      action.__raw = ''function() require("aerial").next() end'';
      options.desc = "Next symbol";
    }
    {
      mode = "n";
      key = "[S";
      action.__raw = ''function() require("aerial").prev() end'';
      options.desc = "Previous symbol";
    }
    {
      mode = "n";
      key = "<leader>fs";
      action.__raw = ''require("telescope").extensions.aerial.aerial'';
      options.desc = "Find symbols";
    }
  ];
}
