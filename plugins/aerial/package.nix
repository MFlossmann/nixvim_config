{ pkgs, ...}:
let 
  version = "2.1.0";
in
pkgs.vimUtils.buildVimPlugin {
  inherit version;

  name = "aerial";

  # src = pkgs.fetchFromGitHub {
  #   owner = "stevearc";
  #   repo = "aerial.nvim";
  #   rev = "v${version}";
  #   hash = "sha256:0ip8xmncp82svlbkphlas88xjvzrpzyy5b1c9x06dqbm4ifai0va";
  # };

  src = builtins.fetchTarball {
    url = "http://github.com/stevearc/aerial.nvim/archive/v${version}.tar.gz";
    sha256 = "sha256:0ip8xmncp82svlbkphlas88xjvzrpzyy5b1c9x06dqbm4ifai0va";
  };
}
