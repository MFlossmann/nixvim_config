{ icons, pkgs, ... }:
{
  opts.enable = true;

  extra = {
    packages = [ pkgs.vimPlugins.arrow-nvim ];

    config = ''
      require('arrow').setup({
        show_icons = true,
        leader_key = ';', -- Recommended to be a single key
        buffer_leader_key = 'm', -- Per Buffer Mappings
      })
      
      -- prewritten for later
      -- require("which-key").add({
        -- {";", desc = "Arrow buffers"},
        -- {"m", desc = "Arrow marks"}
      -- })
    '';
  };

}
