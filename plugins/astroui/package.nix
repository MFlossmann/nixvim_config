{ pkgs, ... }:

let
  version = "2.6.0";
in 
pkgs.vimUtils.buildVimPlugin {
  inherit version;

  name = "astroui";

  src = pkgs.fetchFromGitHub {
    owner = "astronvim";
    repo = "astroui";
    rev = "v${version}";
    hash = "sha256-kygFyXSIXQENWxGKgJAf5Aid5kfKWMsqTxckXbI6gyE=";# "sha256-nmcqJq4L6XFrgrORan5x+WCwSfU3FC4D6Zux45YnIUQ=";
  };

  buildInputs = [
      pkgs.vimPlugins.astrocore
    ];
}
