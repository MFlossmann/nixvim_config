_:
{
  opts = {
    enable = true;
    autoEnableSources = true;
    settings = {
      snippet.expand = ''function(args) require("luasnip").lsp_expand(args.body) end'';

      sources = [
        { name = "nvim_lsp"; priority = 1000; }
        { name = "path"; priority = 250; }
        { name = "buffer"; priority = 500; group_index = 2; }
        { name = "luasnip"; priority = 750; }
      ];
          
      mapping = {
        "<Up>" = "cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Select }";
        "<Down>" = "cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Select }";
        "<C-P>" = "cmp.mapping(function()
          if (cmp.core.view:visible() or vim.fn.pumvisible() == 1) then
            cmp.select_prev_item { behavior = cmp.SelectBehavior.Insert }
          else
            cmp.complete()
          end
        end)";
        "<C-N>" = "cmp.mapping(function()
          if (cmp.core.view:visible() or vim.fn.pumvisible() == 1) then
            cmp.select_next_item { behavior = cmp.SelectBehavior.Insert }
          else
            cmp.complete()
          end
        end)";
        "<C-K>" = "cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert }";
        "<C-J>" = "cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert }";
        "<C-U>" = "cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' })";
        "<C-D>" = "cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' })";
        "<C-Space>" = "cmp.mapping(cmp.mapping.complete(), { 'i', 'c' })";
        "<C-Y>" = "cmp.config.disable";
        "<C-E>" = "cmp.mapping { i = cmp.mapping.abort(), c = cmp.mapping.close() }";
        "<CR>" = "cmp.mapping.confirm { select = false }";
        "<Tab>" = ''cmp.mapping(function(fallback)
          local function has_words_before()
            local line, col = (unpack or table.unpack)(vim.api.nvim_win_get_cursor(0))
            return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match "%s" == nil
          end

          if (cmp.core.view:visible() or vim.fn.pumvisible() == 1) then
            cmp.select_next_item()
          elseif vim.snippet and vim.snippet.active { direction = 1 } then
            vim.schedule(function() vim.snippet.jump(1) end)
          elseif has_words_before() then
            cmp.complete()
          else
            fallback()
          end
        end, { 'i', 's' })'';
        "<S-Tab>" = "cmp.mapping(function(fallback)
          if (cmp.core.view:visible() or vim.fn.pumvisible() == 1) then
            cmp.select_prev_item()
          elseif vim.snippet and vim.snippet.active { direction = -1 } then
            vim.schedule(function() vim.snippet.jump(-1) end)
          else
            fallback()
          end
        end, { 'i', 's' })";
      };
    };
  };

  rootOpts = {
    keymaps = [
      {
        mode = "n";
        key = "<leader>uc";
        action.__raw = ''require("astrocore.toggles").buffer_cmp'';
        options.desc = "Toggle autocompletion (buffer)";
      }
      {
        mode = "n";
        key = "<leader>uC";
        action.__raw = ''require("astrocore.toggles").cmp'';
        options.desc = "Toggle autocompletion (global)";
      }
    ];
  };
}
