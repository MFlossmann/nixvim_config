_:{
  opts = {
    enable = true;

    fromLua = [
      { paths = "~/.config/snippets/"; }
    ];
  };

  rootOpts.keymaps = [
    {
      mode = "i";
      key = "<C-g>";
      action.__raw = ''function() require("luasnip").jump(1) end'';
      options.desc = "Jump to next snippet node";
    }
    {
      mode = "i";
      key = "<C-h>";
      action.__raw = ''function() require("luasnip").jump(-1) end'';
      options.desc = "Jump to previous snippet node";
    }
  ];
}
