_: {
  opts.enable = true;

  keymaps = [
    {
      key = "<leader>O";
      action = "<Cmd>Oil<Cr>";
      options.desc = "Toggle oil floating window";
      mode = "n";
    }
    {
      key = "<leader>to";
      action.__raw = "function() require('oil').toggle_float() end";
      options.desc = "Toggle oil floating window";
      mode = "n";
    }
  ];
}
