# homepage: https://github.com/folke/which-key.nvim
# nixvim doc: https://nix-community.github.io/nixvim/plugins/which-key/index.html
{ icons, ... }:

{
  opts = {
    enable = true;


    # Disable which-key when in neo-tree or telescope

    settings = {
      icons.group = "";
      window.border = "single";

      # Customize section names (prefixed mappings)
      spec = [
        { __unkeyed = "<leader>b"; group = "Buffers"; icon = icons.Tab; }
        { __unkeyed = "<leader>bs"; group = "Sort Buffers"; icon = icons.Sort;}
        { __unkeyed = "<leader>d"; group = "Debugger"; icon = icons.Debugger;}
        { __unkeyed = "<leader>f"; group = "Find"; icon = icons.Search;}
        { __unkeyed = "<leader>g"; group = "Git"; icon = icons.Git;}
        { __unkeyed = "<leader>l"; group = "Language Tools"; icon = icons.ActiveLSP;}
        { __unkeyed = "<leader>m"; group = " Markdown"; }
        { __unkeyed = "<leader>s"; group = "Session"; icon = icons.Session;}
        { __unkeyed = "<leader>t"; group = "Terminal"; icon = icons.Terminal;}
        { __unkeyed = "<leader>u"; group = "UI/UX"; icon = icons.Window;}
      ];

      disable.ft = [
        "TelescopePrompt"
        "neo-tree"
        "neo-tree-popup"
      ];
    };


    # registrations = {
    #   "<leader>b".name = "${icons.Tab} Buffers";
    #   "<leader>bs".name = "${icons.Sort} Sort Buffers";
    #   "<leader>d".name = "${icons.Debugger} Debugger";
    #   "<leader>f".name = "${icons.Search} Find";
    #   "<leader>g".name = "${icons.Git} Git";
    #   "<leader>l".name = "${icons.ActiveLSP} Language Tools";
    #   "<leader>m".name = " Markdown";
    #   "<leader>s".name = "${icons.Session} Session";
    #   "<leader>t".name = "${icons.Terminal} Terminal";
    #   "<leader>u".name = "${icons.Window} UI/UX";
    # };
  };

  # Enable catppuccin colors
  # https://github.com/catppuccin/nvim/blob/main/lua/catppuccin/groups/integrations/which_key.lua
  rootOpts.colorschemes.catppuccin.settings.integrations.which_key = true;
}
