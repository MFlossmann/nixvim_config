{ pkgs, ...}:
let
  version = "b1e70b6fc1682a83929aee63680d2b43456fe9a5";
  owner = "nanotee";
  repo = "zoxide.vim";
in
pkgs.vimUtils.buildVimPlugin {
  name = "zoxide.vim";
  inherit version;
  
  src = builtins.fetchTarball {
    url = "http://github.com/${owner}/${repo}/archive/${version}.tar.gz";
    sha256 = "sha256:025rqfs0n2ryi7xwzpq5h2r7jhvxbrb2gjp6fqa072hapgqr0igb";
  };
  # src = pkgs.fetchFromGitHub {
  #   owner = "nanotee";
  #   repo = "zoxide.vim";
  #   rev = version;
  #   hash = "";
  # };
}
